<?php

namespace App\Http\Controllers;
use App\Models\Paquete;
use Illuminate\Http\Request;

class PaqueteController extends Controller
{    
    public function create(){
        return view('paquetes.create');
    }
    public function store(Request $request) {
        $paquete = new Paquete();
        $paquete->calle= $request->calle;
        $paquete->transportista = $request->transportista;
        $paquete->imagen = $request->imagen->store('imagenes','paquetes');
        $paquete->created_at = now();
        $paquete->save();
        return redirect()->action([TransportistaController::class, 'index']);

    }
}
