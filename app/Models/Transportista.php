<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Transportista extends Model
{
    use HasFactory;


    public function getDate()
    {
        $fechaFormateada = Carbon::parse($this->fechaPermisoConducir);
        return $fechaFormateada->diffInYears(Carbon::now());
    }

    public function getRouteKeyName()
    {
        return 'slug';
    }
}
