<?php

namespace Database\Seeders;

use App\Models\Empresa;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        
        DB::table('empresas')->delete();
        $this->call(EmpresaSeeder::class);
        \App\Models\Empresa::factory(20)->create();

        DB::table('paquetes')->delete();
        $this->call(PaqueteSeeder::class);
        \App\Models\Paquete::factory(20)->create();
        
        DB::table('transportistas')->delete();
        $this->call(TransportistaSeeder::class);

}

}