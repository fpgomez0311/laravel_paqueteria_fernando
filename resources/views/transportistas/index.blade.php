@extends('layouts.master')
@section('titulo')
Transportistas
@endsection
@section('contenido')

<br><br>
<div class="row">
@foreach( $transportistas as $clave => $transportista )
<div class="card" style="width:400px">
<a href="{{ route('transportistas.show' , $transportista->id ) }}">
  <img class="card-img-top" src="{{asset('assets/imagenes/transportistas')}}/{{$transportista->imagen}}" alt="Card image">
  <div class="card-body">
    <h4 class="card-title">{{$transportista->nombre}}</h4>
</a>
   0 paquetes pendientes de entrega

  </div>
</div>
@endforeach
</div>

@endsection