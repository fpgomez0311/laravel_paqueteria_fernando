@extends('layouts.master')
@section('titulo')
Ver transportista
@endsection
@section('contenido')
<div class="row">
<div class="col-sm-3"><br>
<figure class="figure">
  <img src="{{asset('assets/imagenes/transportistas')}}/{{$transportistas->imagen}}" class="figure-img img-fluid rounded">
</figure>
</div>
<div class="col-sm-9">
<p class="h1">{{$transportistas->nombre}} {{$transportistas->apellido}}</p>
<p class="h4">Años de permiso de circulación: </p>
<p class="lead">{{$transportistas->getDate()}}</p>
<p class="h4">Empresas:</p>
<p class="lead"></p>
<p class="h4">Paquetes:</p>

</div>
</div>

@endsection